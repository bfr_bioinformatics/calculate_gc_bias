import argparse
import os
import sys

import pysam
from Bio import SeqIO
from Bio import SeqUtils as SU


def get_gc_per_window(fastafile, windowsize):
  """
  Calculates the gc content for all windows in all subsequences of a fasta file.

  :type windowsize: int
  :type fastafile: object
  :returns: list of floats
  """
  
  overall_gcs = []
  window_gcs = []
  for fasta in fastafile:
    sequence = str(fasta.seq)
    reflen = len(sequence)
    overall_gcs.append(SU.GC(sequence))
    for i in range(windowsize, reflen, windowsize):
      window_gcs.append(SU.GC(sequence[i - windowsize:i]))

  return (window_gcs)


def get_cov_per_window(samfile, windowsize):
  """
  Calculates the average coverage bias of all windows in all sequences of a bam file and the average coverage of the whole reference

  :type windowsize: int
  :type samfile: object
  :returns: List of floats
  """

  refseqs = samfile.references
  reflens = []
  windowcovs = []
  for refseq in refseqs:
    reflens.append(samfile.get_reference_length(refseq))
  for i in range(len(refseqs)):
    for windowend in range(windowsize, reflens[i], windowsize):
      sys.stdout.write("\r {}% finished".format(round(windowend / reflens[i] * 100,2)))
      sys.stdout.flush()
      singlecovs = (samfile.count_coverage(refseqs[i], windowend - windowsize, windowend))
      fullcov = 0
      for j in range(0, 4):
        fullcov += sum(singlecovs[j])
      windowcovs.append(fullcov / windowsize)
  print("\n")
  overallcov = sum(windowcovs) / len(windowcovs)
  relative_windowcovs = [x / overallcov for x in windowcovs]
  samfile.close()

  return (relative_windowcovs)


def calculate_gc_bias(lowerbound, upperbound, gc_per_window, cov_per_window):
  """
  Calculates the coverage bias of windows with GC content in the provided bounds.

  :type upperbound: float
  :type lowerbound: float
  :type cov_per_window: list
  :type gc_per_window: list
  :returns: float
  """

  covbiases = []
  for window in range(min(len(gc_per_window), len(cov_per_window))):
    sys.stdout.write("\r {}% finished".format(round(200*window/len(cov_per_window)*100,2)))
    sys.stdout.flush()

    if lowerbound <= gc_per_window[window] <= upperbound:
      covbiases.append(cov_per_window[window])
  try:
    covbias = (sum(covbiases) / len(covbiases))
  except ZeroDivisionError:
    print("No coverage in provided GC range. Bias will be set to NA")
    return "NA"
  print("\n")

  return covbias

def map_to_ref(read1, read2, ref, bamfile, sample):
  #print("mapping")
  os.system("minimap2 -ax sr {ref} {read1} {read2} | samtools view -Sb > {bamfile}".format(ref=ref, read1=read1, read2=read2, bamfile=bamfile))
  #print("sorting")
  os.system("samtools sort {} -o {}_sorted.bam".format(bamfile, sample))
  #print("indexing")
  os.system("samtools index {}_sorted.bam".format(sample))
  return

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-s', '--samplesheet', help='input samplesheet file', required=True, type=os.path.abspath)
  parser.add_argument('-o', '--outfile', help='output file', required=True, type=os.path.abspath)
  parser.add_argument('-w', '--windowsize',
                      help='size of the window to calculate gc bias and coverage from; default = 200',
                      required=False, default=200, type=int)
  parser.add_argument('-u', '--upperbound', help='upper bound of low GC ranges [percent]; default = 35',
                      required=False, default=35, type=float)
  parser.add_argument('-l', '--lowerbound', help='lower bound of low GC ranges [percent]; default = 25',
                      required=False, default=25, type=float)
  args = parser.parse_args()

  outfile = open(args.outfile, 'w')

  for line in open(args.samplesheet, "r"):
    sample, read1, read2, ref, kit = line.split()
    if read1 == "fq1":
      continue
    bamfile = sample+".bam"
    print("mapping reads from sample {sample} to reference {ref}".format(sample=sample, ref=ref))
    if not os.path.isfile(sample+"_sorted.bam"):
      map_to_ref(read1, read2, ref, bamfile, sample)

    bamfile = sample+"_sorted.bam"
    samfile = pysam.AlignmentFile(bamfile, 'rb')
    fastafile = SeqIO.parse(open(ref), 'fasta')
    windowsize = args.windowsize
    print("calculating gc content per window")

    gc_per_window = get_gc_per_window(fastafile, windowsize)

    print("calculating coverage per window")
    cov_per_window = get_cov_per_window(samfile, windowsize)

    print("calculating gc bias")
    gcbias = calculate_gc_bias(args.lowerbound, args.upperbound, gc_per_window, cov_per_window)
    print("{sample} \t {gcbias} \t {kit}".format(sample=sample, gcbias=gcbias,  kit=kit))
    outfile.write("{sample} \t {gcbias} \t {kit}".format(sample=sample, gcbias=gcbias,  kit=kit))


main()
