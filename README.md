calculate_gc_bias calculates the coverage bias of low-GC-regions. It maps a given set of fastq files to provided references. Then it iterates over the resulting .bam files in steps of the provided windowsize without overlaps and calculates both the GC content and the coverage of the given window. Finally all windows with a GC content in the provided bounds are selected and their coverage bias is reported to stdout and written to user-provided a result file.


#### Dependencies
calculate_gc_bias will need biopython, minimap2 and samtools installed. 

#### Execution

```
usage: calculate_gc_bias.py [-h] -s SAMPLESHEET -o OUTFILE [-w WINDOWSIZE]
                            [-u UPPERBOUND] [-l LOWERBOUND]

optional arguments:
  -h, --help            show this help message and exit
  -s SAMPLESHEET, --samplesheet SAMPLESHEET
                        input samplesheet file
  -o OUTFILE, --outfile OUTFILE
                        output file
  -w WINDOWSIZE, --windowsize WINDOWSIZE
                        size of the window to calculate gc bias and coverage
                        from; default = 200
  -u UPPERBOUND, --upperbound UPPERBOUND
                        upper bound of low GC ranges [percent]; default = 35
  -l LOWERBOUND, --lowerbound LOWERBOUND
                        lower bound of low GC ranges [percent]; default = 25

```

#### Configuration

The script is run for batches of samples. Therefore the input files are provided via a tab-seperated samplesheet in the following format:

| sample  | fq1                         | fq2                         | reference                 | kit  |
|---------|-----------------------------|-----------------------------|---------------------------|------|
| sample1 | /path/to/reads1_R1.fastq.gz | /path/to/reads1_R2.fastq.gz | /path/to/reference1.fasta | XT   |
| sample2 | /path/to/reads2_R1.fastq.gz | /path/to/reads2_R2.fastq.gz | /path/to/reference2.fasta | Flex |

A template samplesheet is provided with the code. 


Contact
-------
Please consult the [project gitlab repository](https://gitlab.com/bfr_bioinformatics/calculate_gc_bias) for questions.

If this does not help, please feel free to consult:
 * Simon H. Tausch <Simon.Tausch (at) bfr.bund.de>
